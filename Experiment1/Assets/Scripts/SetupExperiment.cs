﻿using UnityEngine;
using System.Collections;
using MiddleVR_Unity3D;

public class SetupExperiment : MonoBehaviour {
	
	string trials_fname;
	
	SingleTrial[] trials;
	GameObject[] slots;
	GameObject[] worldObjects;

	int state;
	double start_time;
	int trial;
	int num_trials;
	
	//string positionLog;
	string eventLog;
	bool allow_press;
	uint button_pressed;
	
	double time_glass_shown;
	double time_circle_shown;
	double time_button_pressed;
	
	float reaction_time; 
	
	void HideAllObjects()
	{
		foreach(GameObject g in worldObjects)
		{
		   g.GetComponent<Renderer>().enabled=false;
		}
	}
		
	void SetupObjects()
	{
		worldObjects=new GameObject[16];
		
		worldObjects[0]=GameObject.Find ("green_apple"); // 7
		worldObjects[1]=GameObject.Find ("green_coffee");// 7.7
		worldObjects[2]=GameObject.Find ("green_milk");  // 10.1
		worldObjects[3]=GameObject.Find ("green_peanut_butter"); //9.5
		worldObjects[4]=GameObject.Find ("green_tea"); // 1.2
		worldObjects[5]=GameObject.Find ("red_apple");
		worldObjects[6]=GameObject.Find ("red_coffee");
		worldObjects[7]=GameObject.Find ("red_milk");
		worldObjects[8]=GameObject.Find ("red_peanut_butter");
		worldObjects[9]=GameObject.Find ("red_tea");
		
		worldObjects[10]=GameObject.Find ("target_red_soda");
		worldObjects[11]=GameObject.Find ("target_red_yogurt");
		worldObjects[12]=GameObject.Find ("side_red_soda");
		worldObjects[13]=GameObject.Find ("side_red_yogurt");
		worldObjects[14]=GameObject.Find ("glass_regular");
		worldObjects[15]=GameObject.Find ("glass_different");	
		
		float yogurt_height=worldObjects[10].GetComponent<Renderer>().bounds.max.y-worldObjects[10].GetComponent<Renderer>().bounds.min.y;
		float yogurt_x=worldObjects[10].GetComponent<Renderer>().bounds.max.x-worldObjects[10].GetComponent<Renderer>().bounds.min.x;
		float yogurt_z=worldObjects[10].GetComponent<Renderer>().bounds.max.z-worldObjects[10].GetComponent<Renderer>().bounds.min.z;

		for(int i=0;i<worldObjects.Length;i++)
		{
			float xsize=worldObjects[i].GetComponent<Renderer>().bounds.max.x-worldObjects[i].GetComponent<Renderer>().bounds.min.x;
			float ysize=worldObjects[i].GetComponent<Renderer>().bounds.max.y-worldObjects[i].GetComponent<Renderer>().bounds.min.y;
			float zsize=worldObjects[i].GetComponent<Renderer>().bounds.max.z-worldObjects[i].GetComponent<Renderer>().bounds.min.z;
			
			print (worldObjects[i].name + ": " + xsize + " " + ysize+ " " + zsize);
			float yscale=yogurt_height/ysize;
			//print ("  scale should be: " + yscale);
		}		
		
		
	    HideAllObjects();
	}
	
	
	void SetupSlots ()
	{
		slots=new GameObject[9];
		
		for(int i=0;i<9;i++)
		{
			string objectToFind="Position"+(i).ToString();
			slots[i]=GameObject.Find(objectToFind);
		}
		
		Vector3 z_inc=new Vector3(0.0f,0.0f,0.1f);
		Vector3 x_inc=new Vector3(0.1f,0.0f,0.0f);
		
		float x_circle=Mathf.Sqrt(3.0f)*0.5f;
		float z_circle=0.5f;
		
		Vector3 basepos;
		Vector3 newpos;
			
		basepos=slots[0].transform.position;
		
		newpos=basepos;
		newpos+=z_inc;		
		slots[1].transform.position=newpos;
		
		newpos=basepos;
		newpos+=z_inc*z_circle+x_inc*x_circle;		
		slots[2].transform.position=newpos;
		
		newpos=basepos;
		newpos+=-z_inc*z_circle+x_inc*x_circle;		
		slots[3].transform.position=newpos;
		
		newpos=basepos;
		newpos-=z_inc;		
		slots[4].transform.position=newpos;
		
		newpos=basepos;
		newpos+=-z_inc*z_circle-x_inc*x_circle;		
		slots[5].transform.position=newpos;
		
		newpos=basepos;
		newpos+=z_inc*z_circle-x_inc*x_circle;		
		slots[6].transform.position=newpos;
		
		newpos=basepos;
		newpos-=x_inc*1.75f;		
		slots[7].transform.position=newpos;
		
		newpos=basepos;
		newpos+=x_inc*1.75f;		
		slots[8].transform.position=newpos;
		
	}		
		
	void LoadTrials()
	{
		trials=new SingleTrial[200]; 

	    string fname=Application.streamingAssetsPath+"/"+trials_fname;
		
		string text = System.IO.File.ReadAllText(fname);
		char[] delimiterNewLine = { '\n' };
		char[] delimiterComma = { ',' };
		
		string[] lines=text.Split(delimiterNewLine);
		
		int count=0;
		
		foreach(string s in lines)
		{
			if(s.Length==0) continue;
			
			string[] vals=s.Split(delimiterComma);
			
			SingleTrial st    = new SingleTrial();
			
			st.is_target_soda = int.Parse(vals[0])==1;
			st.target_location= int.Parse(vals[1])-1;
			st.is_side_soda   = int.Parse(vals[2])==1;
			st.side_location  = int.Parse(vals[3])-1;
			st.is_glass_full  = int.Parse(vals[4])==1;
			
			int[] obj=new int[5];
			
			for(int i=0;i<5;i++)
				obj[i]=int.Parse (vals[5+i]);
			
			st.other_objects=obj;
			st.color=vals[10];
			st.color=st.color.Replace("\\n","");
			st.color=st.color.Replace("\\r","");
			
			trials[count]=st;
			
			count++;
		}
		num_trials=count;
		print ("Num Trials: " + num_trials);
	}
	
	
	public void saveLogs()
	{
		//System.IO.File.WriteAllText("C:/tmp/position_log.csv",positionLog);
		System.IO.File.WriteAllText("C:/tmp/event_log.csv",eventLog);
	}
	
	void ShowObjectAtSlot(GameObject g, int s)
	{
	    Vector3 pos=slots[s].transform.position;
		Vector3 opos=g.transform.position;
		
		pos.y=opos.y; //lets keep the objects y pos
		
		
		g.transform.position=pos;
		
		g.GetComponent<Renderer>().enabled=true;	
		print ("showing: " + g.name + " at slot: " + s.ToString());
	}
	
	void ShowGlass(int which)
	{
		SingleTrial st=trials[which];
		
		GameObject glass;
		
		if(st.is_glass_full==true)
			glass=worldObjects[14];
		else
			glass=worldObjects[15];
		
		ShowObjectAtSlot(glass,0);
	}
	
	void ShowCircularArray(int which)
	{
	   SingleTrial st=trials[which];

	   GameObject target;
	   GameObject side;
		
	   bool [] isUsed=new bool[10];
		
	   for(int i=0;i<10;i++)
			isUsed[i]=false;
		
	   if(st.is_target_soda)
	      target=worldObjects[10];
	   else 
	      target=worldObjects[11];
		
	   if(st.is_side_soda)
	      side=worldObjects[12];
	   else 
	      side=worldObjects[13];	
		
	   ShowObjectAtSlot(target,st.target_location);
	   ShowObjectAtSlot(side,st.side_location);
		
	   int count=0;
		
	   for(int i=1;i<7;i++)
	   {
	      if(i!=st.target_location)
		  {
			 int next_object=st.other_objects[count];
			 ShowObjectAtSlot(worldObjects[next_object],i);
		     count++;
		  }
	   }
	}
	
	void ReadConfig()
	{
	    string fname=Application.streamingAssetsPath+"/config.txt";
		
		string text = System.IO.File.ReadAllText(fname);
		char[] delimiterNewLine = { '\n' };
		
		string[] lines=text.Split(delimiterNewLine);
		
		trials_fname=lines[0];
		print ("Trials filename is: " + trials_fname);
	}
	
	void generateRandom()
	{
		string t="";
		
		bool [] isUsed=new bool[10];
		
		for(int e=0;e<96;e++)
		{
			for(int i=0;i<10;i++)
				isUsed[i]=false;
			
			for(int i=0;i<5;i++)
	    	{
	     		int r=Random.Range(5,10);
			 
				while(isUsed[r]==true) //keep trying to get a random one we haven't used yet
		     	{
					r=Random.Range(5,10);
			 	}
			
			 	t+=r.ToString();
			 	if(i!=4)
					t+=",";
		     	isUsed[r]=true;
		  	}
			t+="\r\n";
	   	}
		System.IO.File.WriteAllText("objects.txt",t);			
	}
	
	
	void Start()
	{
		//generateRandom();
		ReadConfig ();
		SetupSlots();
		LoadTrials();
		SetupObjects ();
		
		trial=0;
		state=0;
		start_time=0.0f;
	}
	
	void logEvent(string what)
	{
		eventLog+=Time.time.ToString()+","+what+"\r\n";		
	}
	
	void logEvent(string what, string extra)
	{
		eventLog+=Time.time.ToString()+","+what+","+extra+"\r\n";		
	}
	
	void logEvent(string what, string extra, string extra2)
	{
		extra2=extra2.Replace("\\n","");
		extra2=extra2.Replace("\\r","");
		eventLog+=Time.time.ToString()+","+what+","+extra+","+extra2; //+"\r\n"; //where is mystery new line coming from?		
	}
	
	// Update is called once per frame
	void Update ()
	{
		/*if(MiddleVR.VRDeviceMgr!=null) //if middlevr not null
		{
			positionLog+=Time.time.ToString()+","+"somedata"+"\r\n";	
		}*/
		
		double current_time=MiddleVR.VRKernel.GetTime()/1000.0; //Time.time; 
		//print (current_time);
		double delta_time=current_time-start_time;
		
	   	if(state == 0)
		{
			if(MiddleVR.VRDeviceMgr!=null)  //if middlevr active (not null)
			{
			    if (MiddleVR.VRDeviceMgr.IsKeyPressed(MiddleVR.VRK_SPACE)) //Input.GetKeyDown("space")) //if()
				{
					//logEvent("SHOW_GLASS",trial.ToString ());
					ShowGlass(trial); 
					start_time=current_time;
					state++;
					return;
				}

				/*GameObject head=GameObject.Find("HeadNode");
				Vector3 head_pos=head.transform.position;
				Vector3 basepos=slots[0].transform.position;
				Vector3 diff=head_pos-basepos;
				float size=diff.magnitude;

				eventLog+="Head pos: " + head_pos.ToString() + "\r\n";
				eventLog+="Slot pos: " + basepos.ToString() + "\r\n";
				eventLog+="Distance: " + size+ "\r\n";*/



			}
		}
		
		if(state == 1) //allow user to see glass for 1 seconds
		{
			if(delta_time>1.0f)
			{
				HideAllObjects();
				ShowCircularArray(trial); //TODO: log more things, basically info from trial
				time_circle_shown=current_time;
				//logEvent ("SHOW_ARRAY",trial.ToString (),trials[trial].make_log_string());
				start_time=current_time;
				state++;
				allow_press=true;
				return;
			}		
		}
		
		if(state == 2) //allow user to see circle stimulus for 2 seconds
		{
			if(allow_press)
			{
				for(uint i=0;i<6;i++)
				{				
					if(MiddleVR.VRDeviceMgr.IsWandButtonPressed(i))
					{
						GameObject dart=GameObject.Find("Dart");
						dart.GetComponent<AudioSource>().Play();

						print ("Pressed Button: " + i.ToString());
						//logEvent ("BUTTON_PRESSED",i.ToString());
						button_pressed=i;
						time_button_pressed=current_time;
						allow_press=false; //only allow one button press
						break;
					}
				}
			}
			
			if(delta_time>2.0f)
			{
				//eventLog+=Time.time.ToString()+",";
				eventLog+=(trial+1).ToString()+",";
				
				
					
				if(allow_press==true) //user never pressed a button
					eventLog+="-1"+",,";
				else
				{
					eventLog+=button_pressed.ToString()+",";
				    
					double reaction_time=(time_button_pressed-time_circle_shown)*1000.0f;
					int reaction_as_int=Mathf.RoundToInt((float)reaction_time);

					eventLog+=reaction_as_int.ToString()+",";
				}
				
				eventLog+=trials[trial].make_log_string();
				
				HideAllObjects(); 
				//logEvent ("HIDE_ARRAY",trial.ToString ());
				start_time=current_time;
				state++;
				return;
			}
			

			
			
		}	
		if(state ==3) //allow user to see empty world for 2 seconds
		{
			if(delta_time>2.0f && ((allow_press==false && MiddleVR.VRDeviceMgr.IsWandButtonPressed(button_pressed)==false) || allow_press))
			{
				trial++;
				if(trial>num_trials)//num_trials) //96
				{
					saveLogs(); 
					state++;
					//logEvent ("COMPLETE");
					print ("trials complete");
					GameObject tada=GameObject.Find("tada");
					tada.GetComponent<AudioSource>().Play();
					/*
					#if UNITY_EDITOR
						UnityEditor.EditorApplication.isPlaying = false;
					#elif UNITY_WEBPLAYER
						public static string webplayerQuitURL = "http://google.com";
						Application.OpenURL(webplayerQuitURL);
					#else
						Application.Quit();
					#endif
					*/
				}
				else
				{
					//logEvent("SHOW_GLASS",trial.ToString ());
					ShowGlass(trial); //TODO log that we show glass here
					start_time=current_time;
					state=1;
					return;
				}
			}
		}
	}
}
