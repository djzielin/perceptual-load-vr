﻿using UnityEngine;
using System.Collections;

public class SingleTrial{
	
	public bool is_target_soda;
	public int  target_location;
	public bool is_side_soda;
	public int side_location;
	public bool is_glass_full;
	public string color;
	
	public int[] other_objects;
	

	
	public string make_log_string()
	{
	    string s="";
		
		if(is_target_soda)
			s+="soda,";
		else
			s+="jogurt,";
		
		s+=target_location.ToString();
		s+=",";
		
		if(is_side_soda)
			s+="soda,";
		else
			s+="jogurt,";
		
		s+=side_location.ToString();
		s+=",";  
		
		s+=color;
		//Debug.Log(s.Contains("\\n")); //why is there a bonus newline?
		
		s=s.Replace("\\n","");
		s=s.Replace("\\r","");
		
		return s;
	}
	

}
